package app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import app.donation.R;
import app.main.DonationApp;
import app.models.SessionManagement;

public class Login extends Activity
{
	SessionManagement session;
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    session = new SessionManagement(getApplicationContext());
  }


  public void signinPressed (View view) 
  {
    DonationApp app = (DonationApp) getApplication();

    TextView email     = (TextView)  findViewById(R.id.loginEmail);
    TextView password  = (TextView)  findViewById(R.id.loginPassword);

 // Get username, password from EditText
    String inputusername = email.getText().toString();
    String inputpassword = password.getText().toString();
    
     
    // Validate if username, password is filled            
    if(inputusername.trim().length() > 0 && inputpassword.trim().length() > 0){
         
        // For testing puspose username, password is checked with static data
        // username = admin
        // password = admin
         
        if(app.validUser(email.getText().toString(), password.getText().toString())){
             
            // Creating user login session
            // Statically storing name="Android Example"
            // and email="androidexample84@gmail.com"
            session.createLoginSession(inputusername);
             
            // Staring MainActivity
            Intent i = new Intent(getApplicationContext(), Donate.class);
            startActivity(i);
            finish();
             
        }else{
             
            // username / password doesn't match&
            Toast.makeText(getApplicationContext(),
              "Username/Password is incorrect",
                    Toast.LENGTH_LONG).show();
             
        }              
    }else{
         
        // user didn't entered username or password
        Toast.makeText(getApplicationContext(),
             "Please enter username and password",
                  Toast.LENGTH_LONG).show();
         
    }
  }
}