package app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import app.donation.R;
import app.main.DonationApp;
import app.models.SessionManagement;
import app.models.User;


public class Signup extends Activity {

    // Session Manager Class
    SessionManagement session;
    
    EditText inputfirstName;

	EditText inputlastName;

	EditText inputEmail;

	EditText inputPassword;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		// Session class instance
        session = new SessionManagement(getApplicationContext());
         
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signup, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void registerPressed (View view) 
	{
		inputfirstName = (EditText)  findViewById(R.id.firstName);
		inputlastName  = (EditText)  findViewById(R.id.lastName);
		inputEmail     = (EditText)  findViewById(R.id.Email);
		inputPassword  = (EditText)  findViewById(R.id.Password);

/*		User user = new User (firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());

		DonationApp app = (DonationApp) getApplication();
		app.newUser(user);*/
        
        User user = new User (inputfirstName.getText().toString(), inputlastName.getText().toString(), inputEmail.getText().toString(), inputPassword.getText().toString());
        
        DonationApp app = (DonationApp) getApplication();
        app.newUser(user);
		

        Intent setEnviroment = new Intent(getApplicationContext(), Welcome.class);
        startActivity(setEnviroment);
        finish();
	}
}

